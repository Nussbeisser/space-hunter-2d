﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : IEquatable<Node>
{
    public bool Walkable { get; private set; }
    public Vector3 WorldPosition { get; private set; }
    public int GridX { get; private set; }
    public int GridY { get; private set; }

    public int CostSoFar { get; set; }
    public int HeuristicCost { get; set; }
    public Node Parent { get; set; }
    public int EstimatedTotalCost { get { return CostSoFar + HeuristicCost; } }

    public Node(bool walkable, Vector3 worldPosition, int gridX, int gridY)
    {
        Walkable = walkable;
        WorldPosition = worldPosition;
        GridX = gridX;
        GridY = gridY;
    }

    public Node(Node other)
    {
        Walkable = other.Walkable;
        WorldPosition = other.WorldPosition;
        GridX = other.GridX;
        GridY = other.GridY;
    }

    public override string ToString()
    {
        return GridX.ToString() + "," + GridY.ToString();
    }

    public bool Equals(Node other)
    {
        if (other == null)
            return false;
        return GridX.Equals(other.GridX) && GridY.Equals(other.GridY);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(obj, null))
            return false;
        if (ReferenceEquals(obj, this))
            return true;
        if (obj.GetType() != GetType())
            return false;
        return Equals(obj as Node);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            int hashCode = 13;
            hashCode = (hashCode * 7) + GridX.GetHashCode();
            hashCode = (hashCode * 7) + GridY.GetHashCode();
            return hashCode;
        }
    }
}
