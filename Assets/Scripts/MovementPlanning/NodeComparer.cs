﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class NodeComparer : Comparer<Node>
{
    public override int Compare(Node x, Node y)
    {
        return x.EstimatedTotalCost.CompareTo(y.EstimatedTotalCost);
    }
}

