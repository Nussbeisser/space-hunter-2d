﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public struct Line
{
    public Vector2 PointA { get; set; }
    public Vector2 PointB { get; set; }

    public Line(Vector2 pointA, Vector2 pointB)
    {
        PointA = pointA;
        PointB = pointB;
    }

    public Vector2 Midpoint
    {
        get
        {
            float x = (PointA.x + PointB.x) / 2;
            float y = (PointA.y + PointB.y) / 2;
            return new Vector2(x, y);
        }
    }

    public bool IsPointOn(Vector2 position)
    {
        return Mathf.Min(PointA.x, PointB.x) <= position.x &&
            Mathf.Max(PointA.x, PointB.x) >= position.x &&
            Mathf.Min(PointA.y, PointB.y) <= position.y &&
            Math.Max(PointA.y, PointB.y) >= position.y;
    }

    public float DistanceTo(Vector2 position)
    {
        Vector2 normalPoint = GetNormalPointFrom(position);
        return Vector2.Distance(normalPoint, position);
    }

    public Vector2 GetNormalPointFrom(Vector2 position)
    {
        Vector2 lineDirection = PointB - PointA;
        lineDirection.Normalize();
        Vector2 pointToLine = position - PointA;
        float dotProduct = Vector2.Dot(lineDirection, pointToLine);
        Vector2 normalPoint = new Vector2();
        normalPoint = dotProduct * lineDirection;
        normalPoint += PointA;
        return normalPoint;
    }
}
