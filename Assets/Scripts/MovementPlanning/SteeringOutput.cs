﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class SteeringOutput
    {
        private Vector2 velocity;

        public float Rotation { get; set; }
        public Vector2 Velocity
        {
            get { return velocity.normalized; }
            set { velocity = value; }
        }

        public SteeringOutput()
        {

        }

        public SteeringOutput(Vector2 velocity, float rotation)
        {
            Velocity = velocity;
            Rotation = rotation;
        }

        public override string ToString()
        {
            return Velocity.ToString() + " " + Rotation.ToString();
        }
    }
}
