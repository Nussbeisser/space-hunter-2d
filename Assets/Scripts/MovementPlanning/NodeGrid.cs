﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeGrid : MonoBehaviour, INodeGrid
{
    [SerializeField]
    private LayerMask unwalkableMask;
    [SerializeField]
    private float nodeRadius;
    [SerializeField]
    private Vector2 gridSize;

    private float nodeDiameter;

    public int NoOfGridColumnsX { get; private set; } //NOTE: not public?
    public int NoOfGridRowsY { get; private set; }
    public Node[,] Grid { get; private set; }
    public float NodeRadius { get { return nodeRadius; } }

    public void Start()
    {
        nodeDiameter = nodeRadius * 2;
        NoOfGridColumnsX = Mathf.RoundToInt(gridSize.x / nodeDiameter);
        NoOfGridRowsY = Mathf.RoundToInt(gridSize.y / nodeDiameter);
        CreateGrid();
    }

    private void CreateGrid()
    {
        Grid = new Node[NoOfGridColumnsX, NoOfGridRowsY];
        Vector2 gridBottomLeft = transform.position - Vector3.right * gridSize.x / 2 
            - Vector3.up * gridSize.y / 2;

        IterateGrid(gridBottomLeft);
    }

    private void IterateGrid(Vector2 gridBottomLeft)
    {
        for (int x = 0; x < NoOfGridColumnsX; x++)
            for (int y = 0; y < NoOfGridRowsY ; y++)
            {
                Vector2 gridPoint = gridBottomLeft + Vector2.right * (x * nodeDiameter + nodeRadius)
                    + Vector2.up * (y * nodeDiameter + nodeRadius);

                bool walkable = IsNodeColliding(gridPoint);
                Grid[x, y] = new Node(walkable, gridPoint, x, y);
            }
    }

    private bool IsNodeColliding(Vector2 positionOfNode)
    {
        Collider2D detectedCollider = Physics2D.OverlapCircle(positionOfNode, nodeRadius, unwalkableMask);

        return (detectedCollider == null) ? true : false;
    }

    public Node GetNodeFromWorldPositon(Vector2 worldPosition)
    {
        Vector2 positionPercentage = new Vector2
        {
            x = (gridSize.x / 2 + worldPosition.x) / gridSize.x,
            y = (gridSize.y / 2 + worldPosition.y) / gridSize.y
        };

        positionPercentage = ClampPositonInCaseBeingOutOfGrid(positionPercentage);

        int maxColumnIndex = NoOfGridColumnsX - 1;
        int x = Mathf.RoundToInt(maxColumnIndex * positionPercentage.x);
        int maxRowIndex = NoOfGridRowsY - 1;
        int y = Mathf.RoundToInt(maxRowIndex * positionPercentage.y);

        return Grid[x, y];
    }

    private Vector2 ClampPositonInCaseBeingOutOfGrid(Vector2 positionPercentage)
    {
        Vector2 clampedPositionPercentage = new Vector2()
        {
            x = Mathf.Clamp01(positionPercentage.x),
            y = Mathf.Clamp01(positionPercentage.y)
        };

        return clampedPositionPercentage;
    }

    public Node GetNode(int x, int y)
    {
        return Grid[x, y];
    }
    
    public IEnumerable<Node> GetNeigbouringNodes(Node from)
    {
        List<Node> neighbouringNodes = new List<Node>();

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                    continue;

                int checkX = from.GridX + x;
                int checkY = from.GridY + y;

                if (checkX >= 0 && checkX < NoOfGridColumnsX && checkY >= 0 && checkY < NoOfGridRowsY)
                    neighbouringNodes.Add(Grid[checkX, checkY]);
            }
        }
        return neighbouringNodes;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(gridSize.x, gridSize.y, 1));

        if (Grid != null)
            foreach (Node n in Grid)
            {
                //Handles.Label(n.WorldPosition, n.GridX.ToString() + "," + n.GridY.ToString());
                Gizmos.color = (n.Walkable) ? new Color(0, 0, 1, 0.5f) : new Color(1, 0, 0, 0.5f);
                Gizmos.DrawCube(n.WorldPosition, new Vector3(1, 1, 0.01f) * (nodeDiameter - 0.02f));
            }
    }

}
