﻿using DataStructures;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class OpenList : IEnumerable<Node>
{
    private MinHeap<Node> minHeap;

    public int Count
    {
        get
        {
            return minHeap.Count;
        }
    }

    public OpenList(Comparer<Node> nodeComparer)
    {
        minHeap = new MinHeap<Node>(nodeComparer);
    }

    public void Add(Node item)
    {
        minHeap.Add(item);
    }

    public bool Contains(Node item)
    {
        return minHeap.GetHeap().Contains(item);
    }

    public IEnumerator<Node> GetEnumerator()
    {
        return minHeap.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
       return minHeap.GetEnumerator();
    }

    public Node GetSmallestElement()
    {
        return minHeap.ExtractRoot();
    }
}

