﻿using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Pathfinder : MonoBehaviour
{
    [SerializeField] private bool displayNodePath;
    [SerializeField] private float pathRadius;

    private NodeGrid nodeGrid;
    private NodeComparer nodeComparer;
    private Node startNode;
    private Node goalNode;
    private List<Node> nodePath;

    public Path Path { get; private set; }

    private void Awake()
    {
        nodeGrid = GetComponent<NodeGrid>();
        nodeComparer = new NodeComparer();
        displayNodePath = false;
    }

    public void FindPath(Vector2 characterPosition, Vector2 goalPostion)
    {
        //Stopwatch stopwatch = new Stopwatch();
        //stopwatch.Start();

        goalNode = nodeGrid.GetNodeFromWorldPositon(goalPostion);
        startNode = nodeGrid.GetNodeFromWorldPositon(characterPosition);

        if (!goalNode.Walkable)
        {
            UnityEngine.Debug.Log("goal node unreachable: unwalkable node");
            return;
        }

        OpenList open = new OpenList(nodeComparer);
        List<Node> closed = new List<Node>();

        open.Add(startNode);

        Node currentNode;
        while (open.Count > 0)
        {
            currentNode = open.GetSmallestElement();
            closed.Add(currentNode);

            if (currentNode.Equals(goalNode))
            {
                //stopwatch.Stop();
                //UnityEngine.Debug.Log("Path found in: " + stopwatch.ElapsedMilliseconds.ToString() + " ms");
                nodePath = new List<Node>(RetracePath());
                Path = new Path(nodePath, pathRadius);
                return;
            }

            List<Node> neighbours = new List<Node>(nodeGrid.GetNeigbouringNodes(currentNode));
            foreach (Node neighbour in neighbours)
            {
                if (!neighbour.Walkable || closed.Contains(neighbour))
                    continue;

                int newCostSoFarToNeighbour = currentNode.CostSoFar + GetDistance(currentNode, neighbour);
                if (newCostSoFarToNeighbour < neighbour.CostSoFar || !open.Contains(neighbour))
                {
                    neighbour.CostSoFar = newCostSoFarToNeighbour;
                    neighbour.HeuristicCost = GetDistance(neighbour, goalNode);
                    neighbour.Parent = currentNode;

                    if (!open.Contains(neighbour))
                        open.Add(neighbour);
                }
            }
        }
    }

    private IEnumerable<Node> RetracePath()
    {
        List<Node> path = new List<Node>();
        Node current = goalNode;
        while (!current.Equals(startNode))
        {
            path.Add(current);
            current = current.Parent;
        }

        path.Reverse();
        return path;
    }

    private int GetDistance(Node from, Node to)
    {
        int distanceX = Mathf.Abs(from.GridX - to.GridX);
        int distanceY = Mathf.Abs(from.GridY - to.GridY);

        if (distanceX > distanceY)
            return 14 * distanceY + 10 * (distanceX - distanceY);
        else
            return 14 * distanceX + 10 * (distanceY - distanceX);
    }

    private void OnDrawGizmos()
    {
        if (Path != null)
        {
            Gizmos.DrawCube(Path[0], new Vector3(0.25f, 0.25f));
            for (int i = 0; i < Path.Count - 1; i++)
            {
                Gizmos.DrawCube(Path[i + 1], new Vector3(0.25f, 0.25f));
                Gizmos.DrawLine(Path[i], Path[i + 1]);
            }
        }
        if (nodePath != null && displayNodePath == true)
        {
            foreach (Node n in nodePath)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawSphere(n.WorldPosition, 0.25f);
            }
        }
    }
}
