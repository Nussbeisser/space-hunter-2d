﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Path : IEnumerable<Vector2>
{
    public float Radius { get; private set; }
    public List<Vector2> Waypoints { get; private set; }
    public int Count { get { return Waypoints.Count; } }

    public Path(List<Node> nodePath, float radius)
    {
        Waypoints = new List<Vector2>();
        SimplifyPath(nodePath);
        Radius = radius;
    }

    private void SimplifyPath(List<Node> nodePath)
    {
        Vector2 oldDirection = Vector2.zero;
        for (int i = 0; i < nodePath.Count - 1; i++)
        {
            Vector2 newDirection = GetDirection(nodePath[i + 1], nodePath[i]);

            if (newDirection != oldDirection)
                Waypoints.Add(nodePath[i].WorldPosition);

            oldDirection = newDirection;
        }
        Waypoints.Add(nodePath[nodePath.Count - 1].WorldPosition);
    }

    private Vector2 GetDirection(Node nodeA, Node nodeB)
    {
        int directionX = nodeA.GridX - nodeB.GridX;
        int directionY = nodeA.GridY - nodeB.GridY;
        return new Vector2(directionX, directionY);
    }

    public bool IsWithinPathRadius(Vector2 position, Line currentSegment)
    {
        Vector2 normalPoint = Geometry.GetNormalPoint(currentSegment, position);
        bool pointIsOnPathSegment = currentSegment.IsPointOn(normalPoint);
        float distance = Vector2.Distance(normalPoint, position);

        return distance < Radius && pointIsOnPathSegment;
    }

    public Line GetClosestPathSegment(Vector2 position)
    {
        Line pathSegment = new Line();
        float bestDistance = 10000;
        for (int i = 0; i < Waypoints.Count - 1; i++)
        {
            Vector2 normalPoint = Geometry.GetNormalPoint(new Line(Waypoints[i], Waypoints[i + 1]), position);

            if (!Geometry.IsPointOnLine(Waypoints[i], Waypoints[i + 1], normalPoint))
                normalPoint = Waypoints[i];

            float currentDistance = Vector2.Distance(normalPoint, position);
            if (currentDistance < bestDistance)
            {
                pathSegment = new Line(Waypoints[i], Waypoints[i + 1]);
                bestDistance = currentDistance;
            }
        }

        return pathSegment;
    }

    public Vector2 this[int index]
    {
        get { return Waypoints[index]; }
    }

    public IEnumerator<Vector2> GetEnumerator()
    {
        return Waypoints.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}
