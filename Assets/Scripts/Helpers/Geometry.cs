﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public abstract class Geometry
{
    public static Vector2 GetNormalPoint(Line line, Vector2 point)
    {
        Vector2 lineDirection = line.PointB - line.PointA;
        lineDirection.Normalize();
        Vector2 pointToLine = point - line.PointA;
        float dotProduct = Vector2.Dot(lineDirection, pointToLine);
        Vector2 normalPoint = new Vector2();
        normalPoint = dotProduct * lineDirection;
        normalPoint += line.PointA;
        return normalPoint;
    }

    public static Vector2 GetMidpoint(Vector2 pointA, Vector2 pointB)
    {
        float x = (pointA.x + pointB.x) / 2;
        float y = (pointA.y + pointB.y) / 2;
        return new Vector2(x, y);
    }

    public static bool IsPointOnLine(Vector2 linePointA, Vector2 linePointB, Vector2 pointC)
    {
        return Mathf.Min(linePointA.x, linePointB.x) <= pointC.x &&
            Mathf.Max(linePointA.x, linePointB.x) >= pointC.x &&
            Mathf.Min(linePointA.y, linePointB.y) <= pointC.y &&
            Math.Max(linePointA.y, linePointB.y) >= pointC.y;
    }

    public static float GetDistance(Line line, Vector2 point)
    {
        Vector2 normalPoint = GetNormalPoint(line, point);
        return Vector2.Distance(normalPoint, point);
    }

    public static Vector2 RotateVectorZ(Vector2 vectorToRotate, int ZAxisAngle)
    {
        Quaternion rotation = Quaternion.Euler(0, 0, ZAxisAngle);
        return rotation * vectorToRotate;
    }
}

