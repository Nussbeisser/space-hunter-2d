﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class Extensions
{
    public static Vector2 Sub(this Vector3 vector3, Vector2 subtracted)
    {
        return new Vector2(vector3.x, vector3.y) - subtracted;
    }

    public static Vector2 Add(this Vector3 vector3, Vector2 added)
    {
        return new Vector2(vector3.x, vector3.y) + added;
    }

    public static Vector2 Sub(this Vector2 vector2, Vector3 substracted)
    {
        return vector2 - new Vector2(substracted.x, substracted.y);
    }

    public static Vector2 Add(this Vector2 vector2, Vector3 added)
    {
        return vector2 + new Vector2(added.x, added.y);
    }
}
