﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpController : MonoBehaviour
{
    [SerializeField]
    private int value;
    
    public int Value
    {
        get { return value; }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (IsColliderPlayer(collider) || IsColliderEnemy(collider))
            Destroy(gameObject);
    }

    private bool IsColliderPlayer(Collider2D collider)
    {
        return collider.CompareTag("Player");
    }

    private bool IsColliderEnemy(Collider2D collider)
    {
        return collider.CompareTag("Enemy");
    }
}
