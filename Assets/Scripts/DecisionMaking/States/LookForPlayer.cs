﻿using DecisionMaking;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DecisionMaking.States
{
    public class LookForPlayer : IState<EnemyController>
    {
        private static LookForPlayer instance;
        private Patrol patrol;
        private EnemyController owner;

        private LookForPlayer()
        {
            if (instance != null)
                return;
            instance = this;
        }

        public static LookForPlayer Instance
        {
            get
            {
                if (instance == null)
                    new LookForPlayer();

                return instance;
            }
        }

        public void EnterState(EnemyController owner)
        {

            patrol = new Patrol(owner);
            if (owner.KinematicMover.Velocity == Vector2.zero)
                owner.KinematicMover.Velocity = Vector2.right * owner.MaxSpeed;
            EnemyFieldOfView.PlayerSpotted += OnPlayerSpotted;
            this.owner = owner;
        }

        public void ExitState(EnemyController owner)
        {
            EnemyFieldOfView.PlayerSpotted -= OnPlayerSpotted;
        }

        public void FixedUpdateState(EnemyController owner)
        {
            owner.KinematicMover.FaceValocity();
            patrol.Patrolling();
        }

        public void UpdateState(EnemyController owner)
        {
            return;
        }

        private void OnPlayerSpotted()
        {
            owner.StateMachine.ChangeState(PlayerSpotted.Instance);
        }
    }
}
