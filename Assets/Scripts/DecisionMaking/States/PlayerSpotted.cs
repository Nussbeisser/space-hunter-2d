﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace DecisionMaking.States
{
    public class PlayerSpotted : IState<EnemyController>
    {
        private static PlayerSpotted instance;
        private EnemyFieldOfView eFoV;
        private Vector2 playerLastKnownPosition;
        private Pathfinder pathfinder;
        private bool pathIsNotSet;
        private Transform enemyTransform;
        private PathFollower pathFollower;

        private PlayerSpotted()
        {
            if (instance != null)
                return;
            instance = this;
        }

        public static PlayerSpotted Instance
        {
            get
            {
                if (instance == null)
                    new PlayerSpotted();

                return instance;
            }
        }

        public void EnterState(EnemyController owner)
        {
            owner.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            eFoV = owner.GetComponent<EnemyFieldOfView>();
            EnemyFieldOfView.LostVisual += OnPlayerLostVisual;
            pathfinder = GameObject.Find("Pathfinding").GetComponent<Pathfinder>();
            enemyTransform = owner.gameObject.transform;
            pathFollower = owner.gameObject.GetComponent<PathFollower>();
        }

        public void ExitState(EnemyController owner)
        {
            EnemyFieldOfView.LostVisual -= OnPlayerLostVisual;
        }

        public void FixedUpdateState(EnemyController owner)
        {
            if (eFoV.PlayerIsVisible)
            {
                owner.KinematicMover.FaceAt(eFoV.PlayerPosition);
                pathIsNotSet = true;
            }
            else if (Vector2.Distance(owner.transform.position, playerLastKnownPosition) > 1)
            {
                if (pathIsNotSet)
                {
                    pathIsNotSet = false;
                    pathfinder.FindPath(enemyTransform.position, playerLastKnownPosition);
                    pathFollower.Path = pathfinder.Path;
                }
                pathFollower.FollowPath();
            }
            else
            {
                owner.StateMachine.ChangeState(LookForPlayer.Instance);
                pathIsNotSet = true;
            }
        }

        public void UpdateState(EnemyController owner)
        {
            return;
        }

        private void OnPlayerLostVisual(Vector2 position)
        {
            playerLastKnownPosition = position;
        }
    }
}
