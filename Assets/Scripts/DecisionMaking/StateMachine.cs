﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DecisionMaking
{
    public class StateMachine<T>
    {
        public IState<T> CurrentState { get; private set; }
        public T Owner { get; private set; }

        public StateMachine(T owner)
        {
            Owner = owner;
            CurrentState = null;
        }

        public void ChangeState(IState<T> newState)
        {
            if (CurrentState != null)
                CurrentState.ExitState(Owner);
            CurrentState = newState;
            CurrentState.EnterState(Owner);
        }

        public void Update()
        {
            if (CurrentState != null)
                CurrentState.UpdateState(Owner);
        }

        public void FixedUpdate()
        {
            if (CurrentState != null)
                CurrentState.FixedUpdateState(Owner);
        }
    }
}
