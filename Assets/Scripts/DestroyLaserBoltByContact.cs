﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyLaserBoltByContact : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (IsColiderNotAnotherBolt(collider) && IsColiderNotPickUp(collider))
            Destroy(gameObject);
    }

    private bool IsColiderNotAnotherBolt(Collider2D collider)
    {
        return !collider.CompareTag("LaserBoltEnemy");
    }

    private bool IsColiderNotPickUp(Collider2D collider)
    {
        return !collider.CompareTag("PickUp");
    }
}
