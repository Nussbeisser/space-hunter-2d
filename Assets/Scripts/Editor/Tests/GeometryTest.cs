﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

namespace Tests
{
    public class GeometryTest
    {
        [Test]
        public void GetNormalPointTest()
        {
            Line line = new Line
            {
                PointA = new Vector2(1, 1),
                PointB = new Vector2(4, 1)
            };
            Vector2 point = new Vector2(2, 2);
            Vector2 expected = new Vector2(2, 1);

            Vector2 result = Geometry.GetNormalPoint(line, point);

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void IsPointOnLineTest()
        {
            Line line = new Line
            {
                PointA = new Vector2(1, 1),
                PointB = new Vector2(4, 1)
            };
            Vector2 point = new Vector2(2, 1);

            bool result = Geometry.IsPointOnLine(line.PointA, line.PointB, point);

            Assert.IsTrue(result);
        }
    }
}
