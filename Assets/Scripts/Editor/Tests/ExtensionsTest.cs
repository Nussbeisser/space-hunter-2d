﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

namespace Tests
{
    public class ExtensionsTest
    {
        [Test]
        public void Vector3MinusTest()
        {
            Vector3 vector3 = new Vector3(4, 2);
            Vector2 vector2 = new Vector2(2, 1);
            Vector2 expected = new Vector2(2, 1);

            Vector2 result = vector3.Sub(vector2);

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void Vector3AddTest()
        {
            Vector3 vector3 = new Vector3(4, 2);
            Vector2 vector2 = new Vector2(2, 1);
            Vector2 expected = new Vector2(6, 3);

            Vector2 result = vector3.Add(vector2);

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void Vector2MinusTest()
        {
            Vector2 vector2 = new Vector2(4, 2);
            Vector3 vector3 = new Vector3(2, 1);
            Vector2 expected = new Vector2(2, 1);

            Vector2 result = vector2.Sub(vector3);

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void Vector2AddTest()
        {
            Vector2 vector2 = new Vector2(4, 2);
            Vector3 vector3 = new Vector3(2, 1);
            Vector2 expected = new Vector2(6, 3);

            Vector2 result = vector2.Add(vector3);

            Assert.AreEqual(expected, result);
        }
    }
}