﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

namespace Tests
{
    public class LineTest
    {
        [Test]
        public void GetNormalPointFromTest()
        {
            Line line = new Line
            {
                PointA = new Vector2(1, 1),
                PointB = new Vector2(4, 1)
            };
            Vector2 point = new Vector2(2, 2);
            Vector2 expected = new Vector2(2, 1);

            Vector2 result = line.GetNormalPointFrom(point);

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void DistanceToTest()
        {
            Line line = new Line
            {
                PointA = new Vector2(1, 1),
                PointB = new Vector2(4, 1)
            };
            Vector2 point = new Vector2(2, 2);
            float expected = 1f;

            float result = line.DistanceTo(point);

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void IsPointOnTest()
        {
            Line line = new Line
            {
                PointA = new Vector2(1, 1),
                PointB = new Vector2(4, 1)
            };
            Vector2 point = new Vector2(3, 1);

            Assert.IsTrue(line.IsPointOn(point));
        }

        [Test]
        public void MidpointTest()
        {
            Line line = new Line
            {
                PointA = new Vector2(1, 1),
                PointB = new Vector2(4, 1)
            };
            Vector2 expected = new Vector2(2.5f, 1);

            Vector2 result = line.Midpoint;

            Assert.AreEqual(expected, result);
        }
    }
}
