﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class Points
    {
        private int currentValue;
        private int maxValue;
        private int minValue;

        public Points(int currentValue, int maxValue, int minValue)
        {
            this.currentValue = currentValue;
            this.maxValue = maxValue;
            this.minValue = minValue;
        }

        public int CurrentValue
        {
            get
            {
                return currentValue;
            }
            set
            {
                if (value > maxValue)
                    currentValue = maxValue;
                else if (value < minValue)
                    currentValue = minValue;
                else
                    currentValue = value;
            }
        }
    }
}
