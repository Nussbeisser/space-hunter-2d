﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private GameObject Shot;
    [SerializeField]
    private Transform ShotSpawn;
    //private float FireRate;
    [SerializeField]
    private Text ShieldStatus;
    [SerializeField]
    private Text LaserStatus;

    //public int PowerShieldPoints
    //{
    //    set
    //    {
    //        powerShieldPoints.CurrentValue += value;
    //    }
    //}

    //public int LaserChargePoints
    //{
    //    set
    //    {
    //        laserChargePoints.CurrentValue += value;
    //    }
    //}

    //private float nextFire;
    private AudioSource laserFireSound;
    private Points powerShieldPoints;
    private Points laserChargePoints;
    

    private void Start()
    {
        laserFireSound = GetComponent<AudioSource>();
        powerShieldPoints = new Points(150, 200, 0);
        laserChargePoints = new Points(100, 250, 0);
        UpdateShieldStatus();
        UpdateLaserStatus();        
    }

    private void Update()
    {
        ShootLaser();
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (IsPickUp(collider))
        {
            if (collider.gameObject.name == "laserRecharge")
            {
                laserChargePoints.CurrentValue += collider.GetComponent<PickUpController>().Value;
                UpdateLaserStatus();
            }
            else
            {
                powerShieldPoints.CurrentValue += collider.GetComponent<PickUpController>().Value;
                UpdateShieldStatus();
            }
        }
        else if (IsEnemyLaserBolt(collider))
        {

        }
    }

    private void ShootLaser()
    {
        if (Input.GetMouseButtonDown(0) && laserChargePoints.CurrentValue != 0)
        {
            Instantiate(Shot, ShotSpawn);
            laserChargePoints.CurrentValue--;
            UpdateLaserStatus();
            laserFireSound.Play();
        }
    }

    private void UpdateShieldStatus()
    {
        ShieldStatus.text = "Power Shield: " + powerShieldPoints.CurrentValue.ToString();
    }

    private void UpdateLaserStatus()
    {
        LaserStatus.text = "Laser Charge: " + laserChargePoints.CurrentValue.ToString();
    }

    private bool IsPickUp(Collider2D collider)
    {
        return collider.CompareTag("PickUp");
    }

    private bool IsEnemyLaserBolt(Collider2D collider)
    {
        return collider.CompareTag("LaserBoltEnemy");
    }

}
