﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DecisionMaking
{
    public interface IState<T>
    {
        void EnterState(T owner);
        void ExitState(T owner);
        void UpdateState(T owner);
        void FixedUpdateState(T owner);
    }
}
