﻿using System.Collections.Generic;
using UnityEngine;

public interface INodeGrid
{
    Node[,] Grid { get; }
    int NoOfGridColumnsX { get; }
    int NoOfGridRowsY { get; }
    Node GetNode(int x, int y);
    Node GetNodeFromWorldPositon(Vector2 worldPosition);
    IEnumerable<Node> GetNeigbouringNodes(Node from);
}