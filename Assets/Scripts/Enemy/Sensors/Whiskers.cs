﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Whiskers : MonoBehaviour
{
    [SerializeField] private LayerMask mask;
    [SerializeField] private Transform whiskersCastPoint;
    [SerializeField] private float centerLength;
    [SerializeField] private float sidesLength;
    [SerializeField] private int angle;

    public delegate void WhiskerCollide(RaycastHit2D raycastHit2D);
    public static event WhiskerCollide WhiskerCollided;
    public static event WhiskerCollide Deadlocked;

    public RaycastHit2D MainRayHit { get; private set; }
    public RaycastHit2D RightRayHit { get; private set; }
    public RaycastHit2D LeftRayHit { get; private set; }

    private Vector2 mainRayDirection;
    private Vector2 rightRayDirection;
    private Vector2 leftRayDirection;

    private void FixedUpdate()
    {
        UpdateRaysDirection();
        CastRays();
        if (IsDeadlock())
            OnDeadlocked();
        else
        {
            RaycastHit2D hit = GetColliededWhisker();
            if (hit)
                OnWhiskerCollided(hit);
        }
    }

    private bool IsDeadlock()
    {
        return MainRayHit && RightRayHit && LeftRayHit;
    }

    private RaycastHit2D GetColliededWhisker()
    {
        if (MainRayHit)
            return MainRayHit;
        else if (RightRayHit)
            return RightRayHit;
        else
            return LeftRayHit;
    }

    protected virtual void OnWhiskerCollided(RaycastHit2D hit)
    {
        if (WhiskerCollided != null)
            WhiskerCollided(hit);
    }

    protected virtual void OnDeadlocked()
    {
        if (Deadlocked != null)
            Deadlocked(MainRayHit);
    }

    private void UpdateRaysDirection()
    {
        mainRayDirection = (transform.position - whiskersCastPoint.position).normalized;
        rightRayDirection = Geometry.RotateVectorZ(mainRayDirection, angle);
        leftRayDirection = Geometry.RotateVectorZ(mainRayDirection, -angle);
    }

    private void CastRays()
    {
        MainRayHit = Physics2D.Raycast(whiskersCastPoint.position, mainRayDirection, centerLength, mask);
        RightRayHit = Physics2D.Raycast(whiskersCastPoint.position, rightRayDirection, sidesLength, mask);
        LeftRayHit = Physics2D.Raycast(whiskersCastPoint.position, leftRayDirection, sidesLength, mask);
    }

    private void OnDrawGizmos()
    { 
        Gizmos.DrawRay(whiskersCastPoint.position, mainRayDirection * centerLength);
        Gizmos.DrawRay(whiskersCastPoint.position, rightRayDirection * sidesLength);
        Gizmos.DrawRay(whiskersCastPoint.position, leftRayDirection * sidesLength);

        Gizmos.color = Color.red;
        if (MainRayHit)
            Gizmos.DrawSphere(MainRayHit.point, 0.1f);
        if (RightRayHit)
            Gizmos.DrawSphere(RightRayHit.point, 0.1f);
        if (LeftRayHit)
            Gizmos.DrawSphere(LeftRayHit.point, 0.1f);
    }
}
