﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class EnemyFieldOfView : MonoBehaviour
{
    [SerializeField] private float viewRadius;
    [SerializeField] private LayerMask playerMask;
    [SerializeField] private LayerMask obstacleMask;

    public delegate void PlayerSpot();
    public delegate void LooseVisual(Vector2 lastKnownPosition);
    public static event PlayerSpot PlayerSpotted;
    public static event LooseVisual LostVisual;

    public float ViewRadius { get { return viewRadius; } }
    public bool PlayerIsVisible { get; private set; }
    public Vector2 PlayerPosition { get { return playerPosition; } }

    private Vector2 playerPosition;
    private Vector2 directionToPlayer;
    private Vector2 playerLastKnownPosition;
    private float distanceToPlayer;

    private void Start()
    {
        PlayerIsVisible = false;
        StartCoroutine("LookForPlayerEvery", 0.2f);
    }

    private IEnumerator LookForPlayerEvery(float seconds)
    {
        while (true)
        {
            yield return new WaitForSeconds(seconds);
            FindVisiblePlayer();
            if (PlayerIsVisible)
                UpdatePlayerLastKnowPosition();
        }
    }

    private void FindVisiblePlayer()
    {
        Collider2D playerInViewRadius = Physics2D.OverlapCircle(transform.position, viewRadius, playerMask);

        if (playerInViewRadius)
        {
            playerPosition = playerInViewRadius.transform.position;
            directionToPlayer = playerPosition.Sub(transform.position).normalized;
            distanceToPlayer = Vector2.Distance(transform.position, playerPosition);

            if (!ObstacleInWay() && !PlayerIsVisible)
                OnPlayerSpotted();
            if (ObstacleInWay() && PlayerIsVisible)
                OnLostVisual();
        }
        else if (!playerInViewRadius && PlayerIsVisible)
            OnLostVisual();
    }

    protected virtual void OnPlayerSpotted()
    {
        if (PlayerSpotted != null)
            PlayerSpotted();
        PlayerIsVisible = true;
    }

    private void UpdatePlayerLastKnowPosition()
    {
        playerLastKnownPosition = playerPosition;
    }

    protected virtual void OnLostVisual()
    {
        PlayerIsVisible = false;
        if (LostVisual != null)
            LostVisual(playerLastKnownPosition);
    }

    private bool ObstacleInWay()
    {
        return Physics2D.Raycast(transform.position, directionToPlayer, distanceToPlayer, obstacleMask);
    }

    private void OnDrawGizmos()
    {
        if (PlayerIsVisible)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, playerPosition);
        }
        else
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawCube(playerLastKnownPosition, new Vector3(0.2f, 0.2f));
        }
    }
}
