﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;
using DecisionMaking;
using DecisionMaking.States;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private GameObject shot;
    [SerializeField] private Transform shotSpawn;
    [SerializeField] private float maxSpeed;

    private AudioSource laserFireSound;
    private Points powerShieldPoints;
    private Points laserChargePoints;
    private Rigidbody2D rb;
    private StateMachine<EnemyController> stateMachine;

    public GameObject[] PatrolWaypoints { get; private set; }
    public float MaxSpeed { get { return maxSpeed; } }
    public KinematicMover KinematicMover { get; private set; }
    public StateMachine<EnemyController> StateMachine { get { return stateMachine; } }

    private void Start()
    {
        laserFireSound = GetComponent<AudioSource>();
        powerShieldPoints = new Points(150, 200, 0);
        laserChargePoints = new Points(100, 250, 0);
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = Vector2.zero;
        stateMachine = new StateMachine<EnemyController>(this);
        KinematicMover = new KinematicMover(gameObject);
        PatrolWaypoints = GameObject.FindGameObjectsWithTag("PatrolWaypoint");
        stateMachine.ChangeState(LookForPlayer.Instance);
    }

    private void Update()
    {
        stateMachine.Update();
    }

    private void FixedUpdate()
    {
        stateMachine.FixedUpdate();
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (IsPickUp(collider))
        {
            if (collider.gameObject.name == "laserRecharge")
            {
                laserChargePoints.CurrentValue += collider.GetComponent<PickUpController>().Value;
            }
            else
            {
                powerShieldPoints.CurrentValue += collider.GetComponent<PickUpController>().Value;
            }
        }
        else if (IsPlayerLaserBolt(collider))
        {
            powerShieldPoints.CurrentValue -= collider.GetComponent<LaserBoltController>().Damage;
            if (powerShieldPoints.CurrentValue == 0)
                Destroy(gameObject);
        }
    }

    private void ShootLaser()
    {
        {
            Instantiate(shot, shotSpawn);
            laserChargePoints.CurrentValue--;
            laserFireSound.Play();
        }
    }

    private bool IsPickUp(Collider2D collider)
    {
        return collider.CompareTag("PickUp");
    }

    private bool IsPlayerLaserBolt(Collider2D collider)
    {
        return collider.CompareTag("LaserBoltPlayer");
    }

    public Vector2 GetFuturePositionIn(float seconds)
    {
        return (rb.velocity * maxSpeed * seconds).Add(transform.position);
    }
}
