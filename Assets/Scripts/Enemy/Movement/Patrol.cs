﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Patrol
{
    private EnemyController enemyController;
    private Pathfinder pathfinder;
    private GameObject character;
    private PathFollower pathFollower;

    private bool waypointIsReached;
    private float waypointsRadius;
    private Vector2 currentWaypoint;
    private Path path;

    public Patrol(EnemyController enemyController)
    {
        this.enemyController = enemyController;
        pathfinder = GameObject.Find("Pathfinding").GetComponent<Pathfinder>();
        character = enemyController.gameObject;
        pathFollower = character.GetComponent<PathFollower>();

        waypointIsReached = true;
        waypointsRadius = enemyController.PatrolWaypoints[0].GetComponent<PatrolWaypoint>().Radius;
        currentWaypoint = Vector2.zero;
    }

    public void Patrolling()
    {
        if (waypointIsReached)
        {
            RandomlyPickWaypoint();
            pathfinder.FindPath(character.transform.position, currentWaypoint);
            path = pathfinder.Path;
            pathFollower.Path = path;
        }
        else
            pathFollower.FollowPath();

        CheckIfWaypointReached();
    }

    private void CheckIfWaypointReached()
    {
        if (Vector2.Distance(character.transform.position, currentWaypoint) < waypointsRadius)
            waypointIsReached = true;
        else
            waypointIsReached = false;
    }

    private void RandomlyPickWaypoint()
    {
        Vector2 randomWaypoint = currentWaypoint;
        while (randomWaypoint == currentWaypoint)
        {
            int index = Random.Range(0, 7);
            randomWaypoint = enemyController.PatrolWaypoints[index].transform.position; 
        }
        currentWaypoint = randomWaypoint;
    }
}
