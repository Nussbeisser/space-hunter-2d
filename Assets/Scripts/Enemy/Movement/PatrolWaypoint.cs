﻿using UnityEngine;

public class PatrolWaypoint : MonoBehaviour
{
    [SerializeField] private float radius;

    public float Radius { get { return radius; } }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}