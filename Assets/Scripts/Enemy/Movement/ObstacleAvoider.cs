﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ObstacleAvoider : MonoBehaviour
{
    [SerializeField] private float distanceFromObstacle;
    private KinematicMover kinematicMover;

    private Vector2 target;

    private void Start()
    {
        kinematicMover = new KinematicMover(gameObject);
    }

    private void OnEnable()
    {
        Whiskers.WhiskerCollided += OnWhiskerCollided;
        Whiskers.Deadlocked += DeadlockEscape;
    }

    private void OnDisable()
    {
        Whiskers.WhiskerCollided -= OnWhiskerCollided;
        Whiskers.Deadlocked -= DeadlockEscape;
        kinematicMover.Velocity = Vector2.zero;
    }

    private void OnWhiskerCollided(RaycastHit2D hit)
    {
        target = hit.normal * distanceFromObstacle + hit.point;
        kinematicMover.Seek(target);
    }

    private void DeadlockEscape(RaycastHit2D mainRayHit)
    {
        kinematicMover.Enemy.transform.Rotate(Vector3.forward, 180);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawCube(target, new Vector3(0.2f, 0.2f));
    }
}
