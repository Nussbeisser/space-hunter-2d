﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class KinematicMover
{
    private GameObject enemy;
    private EnemyController enemyController;
    private Rigidbody2D rb;

    public Vector2 Velocity { get { return rb.velocity; } set { rb.velocity = value; } }
    public GameObject Enemy { get { return enemy; }  }

    public KinematicMover(GameObject enemy)
    {
        this.enemy = enemy;
        enemyController = enemy.GetComponent<EnemyController>();
        rb = enemy.GetComponent<Rigidbody2D>();
    }

    public void Seek(Vector2 target)
    {
        Vector2 velocity = GetDirectionToTarget(target);
        velocity.Normalize();
        velocity *= enemyController.MaxSpeed;
        rb.velocity = velocity;
    }

    public void ArriveAt(Vector2 target)
    {
        Vector2 velocity = GetDirectionToTarget(target);
        if (Vector2.Distance(enemy.transform.position, target) > 0.25)
        {
            velocity.Normalize();
            velocity *= enemyController.MaxSpeed;
            rb.velocity = velocity;
        }
        else
            rb.velocity = Vector2.zero;
    }

    public void FaceAt(Vector2 target)
    {
        enemy.transform.rotation = Quaternion.Slerp(enemy.transform.rotation,
            Quaternion.LookRotation(Vector3.forward, GetDirectionToTarget(target)), 0.2f);
    }

    public void FaceValocity()
    {
        Vector2 target = enemy.transform.position.Add(rb.velocity.normalized);
        enemy.transform.rotation = Quaternion.Slerp(enemy.transform.rotation,
            Quaternion.LookRotation(Vector3.forward, GetDirectionToTarget(target)), 0.2f);
    }

    private Vector2 GetDirectionToTarget(Vector2 target)
    {
        return target.Sub(enemy.transform.position);
    }
}

