﻿using Assets.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class PathFollower : MonoBehaviour
{
    [SerializeField] private float targetOffset;
    [SerializeField] private float seconds;
    [SerializeField] private float obstalceAvoidOffset;

    //private Path Path;
    private EnemyController enemyController;
    private Vector2 currentPathPosition;
    private Vector2 target;
    private Vector2 futurePosition;
    private KinematicMover kinematicMover;
    private Rigidbody2D rb;
    private bool ObstacleInWay;
    private RaycastHit2D hit;
    private Vector2 avoidObstacleTarget;

    const float gizmoSphereRadius = 0.25f;

    public Path Path { get; set; }

    private void Awake()
    {
        enemyController = GetComponent<EnemyController>();
        kinematicMover = new KinematicMover(gameObject);
        rb = GetComponent<Rigidbody2D>();
        Whiskers.WhiskerCollided += ObstacleDetected;
        ObstacleInWay = false;
    }

    //public void FollowPath(Path pathToFollow)
    //{
    //    path = pathToFollow;
    //    TrackCurrentPathPosition();
    //    futurePosition = enemyController.GetFuturePositionIn(seconds);
    //    Line pathSegment = path.GetClosestPathSegment(futurePosition);
    //    if (!path.IsWithinPathRadius(futurePosition, pathSegment))
    //    {
    //        GetTarget(futurePosition);
    //        SteeringOutput steering = new SteeringOutput
    //        {
    //            Velocity = target.Sub(transform.position),
    //            Rotation = 0
    //        };
    //        kinematicMover.Seek(target);
    //    }
    //}

    private void ObstacleDetected(RaycastHit2D hit)
    {
        ObstacleInWay = true;
        this.hit = hit;
    }

    private void AvoidObstacle()
    {
        Line currentPathSegment = Path.GetClosestPathSegment(currentPathPosition);
        Vector2 direction = currentPathSegment.PointB - currentPathSegment.PointA;
        direction.Normalize();
        avoidObstacleTarget = currentPathPosition + direction * obstalceAvoidOffset;
        kinematicMover.Seek(avoidObstacleTarget);
    }

    public void FollowPath()
    {
        TrackCurrentPathPosition();
        Line pathSegment = Path.GetClosestPathSegment(futurePosition);
        if (ObstacleInWay)
        {
            AvoidObstacle();
            ObstacleInWay = false;
            return;
        }
        futurePosition = enemyController.GetFuturePositionIn(seconds);
        if (!Path.IsWithinPathRadius(futurePosition, pathSegment))
        {
            GetTarget(futurePosition);
            SteeringOutput steering = new SteeringOutput
            {
                Velocity = target.Sub(transform.position),
                Rotation = 0
            };
            kinematicMover.Seek(target);
        }
    }

    private void TrackCurrentPathPosition()
    {
        Line current = Path.GetClosestPathSegment(transform.position);
        Vector2 normalPoint = Geometry.GetNormalPoint(current, transform.position);
        if (!Geometry.IsPointOnLine(current.PointA, current.PointB, normalPoint))
            normalPoint = current.PointA;

        currentPathPosition = normalPoint;
    }

    private void GetTarget(Vector2 futurePosition)
    {
        Line current = Path.GetClosestPathSegment(futurePosition);
        target = Geometry.GetNormalPoint(current, futurePosition);
        ShiftTargetFurther(current.PointA, current.PointB);
    }


    private void ShiftTargetFurther(Vector2 positonA, Vector2 positionB)
    {
        Vector2 direction = positionB - positonA;
        direction.Normalize();
        direction *= targetOffset;
        target += direction;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(currentPathPosition, gizmoSphereRadius);
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(target, gizmoSphereRadius);
        Gizmos.DrawCube(avoidObstacleTarget, new Vector3(gizmoSphereRadius, gizmoSphereRadius));
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(futurePosition, gizmoSphereRadius);
    }
}
