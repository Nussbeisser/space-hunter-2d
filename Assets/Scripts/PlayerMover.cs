﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMover : MonoBehaviour
{
    [SerializeField]
    private float MovementSpeed;

    private Rigidbody2D rb;
    private Vector3 movement;
    private float mainCamRayLength = 10.0f;


    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        float horizontalMovement = Input.GetAxisRaw("Horizontal");
        float verticalMovement = Input.GetAxisRaw("Vertical");

        Move(horizontalMovement, verticalMovement);
        Turn();
    }

    private void Move(float horizontal, float vertical)
    {
        movement.Set(horizontal, vertical, 0.0f);
        movement = movement.normalized * MovementSpeed * Time.deltaTime;

        rb.MovePosition(transform.position + movement);
    }

    private void Turn()
    {
        Ray mainCamRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hitCoordinates;

        if (Physics.Raycast(mainCamRay, out hitCoordinates, mainCamRayLength))
        {
            Vector2 playerToMousePosition = hitCoordinates.point - transform.position;

            //why there is 90 degree displacement? Vector2.Angle? how is it done?
            //---
            float sign;
            if (hitCoordinates.point.y < transform.position.y)
                sign = -1.0f;
            else
                sign = 1.0f;

            float angle = Vector2.Angle(Vector2.right, playerToMousePosition) * sign;

            rb.MoveRotation(angle - 90.0f);
            //---
        }
    }


}
